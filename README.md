# Exercice #
** Calcul matriciel **

Les matrices et vecteurs sont utilisés dans de nombreuses applications de big data.
La somme de deux matrices de type (m, n), A = (aij) et B = (bij) , 
notée A + B, est à nouveau une matrice (cij) de type (m, n) obtenue en additionnant les éléments correspondants,
i.e., pour tout i, j, cij = aij+ bij

## Question

**Proposez un pseudo-code du Driver, Mapper, map(k,v) et du reducer, 
reduce(k,v) de la somme de deux matrices.**



### Who do I talk to? ###

* semigand@yahoo.fr
* seady