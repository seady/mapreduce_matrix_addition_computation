package stubs;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class MatrixDriver extends Configured implements Tool{

  public static void main(String[] args) throws Exception {

    /*
     * Validate that two arguments were passed from the command line.
     */
	Configuration conf = new Configuration();
	int exitCode = ToolRunner.run(conf, new MatrixDriver(),
			args);
	System.out.println(exitCode);
    }

  public int run(String[] args) throws Exception {
	  
	  if (args.length != 3) {
			System.out
					.printf("Usage: AdditionMatricielle <input dir1> <input dir2> <output dir>\n");
			return -1;
			
		}
  	  
   /*
     * Instantiate a Job object for your job's configuration. 
     */
    Job MatrixJob = new Job(getConf());
    Path firstPath = new Path(args[0]);
	Path sencondPath = new Path(args[1]);
	Path third = new Path(args[2]);

    
    /*
     * Specify the jar file that contains your driver, mapper, and reducer.
     * Hadoop will transfer this jar file to nodes in your cluster running 
     * mapper and reducer tasks.
     */
    MatrixJob.setJarByClass(MatrixDriver.class);
    
    MatrixJob.setJobName("Addition de la Matrice");

    /*
     * Ajout des classes et des inputs
     * le programme prend en entier 2 inputs--ce pourquoi nous avons la classe MultipleInputs
     */
    MultipleInputs.addInputPath(MatrixJob,firstPath,TextInputFormat.class,MatrixMapper1.class);
    MultipleInputs.addInputPath(MatrixJob,sencondPath,TextInputFormat.class,MatrixMapper2.class);
    FileOutputFormat.setOutputPath(MatrixJob,third);
    MatrixJob.setReducerClass(MatrixReducer.class);
    MatrixJob.setCombinerClass(MatrixReducer.class);
    MatrixJob.setMapOutputKeyClass(Text.class);
    MatrixJob.setMapOutputValueClass(IntWritable.class);
    
       /*
     * Start the MapReduce job and wait for it to finish.
     * If it finishes successfully, return 0. If not, return 1.
     */
    boolean success = MatrixJob.waitForCompletion(true);
	return (success ? 0 : 1);
  }
}



