package stubs;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


/*
 * cette classe est la même que MatrixMapper1
 */
public class MatrixMapper2 extends Mapper<LongWritable, Text, Text, IntWritable> {

	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {

		String matrixLine = value.toString();
		String[] theContentOfMatrix = matrixLine.split (" ");
		String key1 = theContentOfMatrix[0] + " " + theContentOfMatrix[1];
		int res = Integer.parseInt(theContentOfMatrix[2]);

		/*
		 * la clé = (i,j)
		 */
		context.write(new Text(key1), new IntWritable(res));
	}
	public void setup(Context context) {


	}
}



