package stubs;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MatrixReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

  @Override
  public void reduce(Text key, Iterable<IntWritable> values, Context context)
		    throws IOException, InterruptedException 
		    {
		        int val = 0;
		        for (IntWritable value : values) 
		        {
		            val = val + value.get();
		        }
		        context.write(key, new IntWritable(val));
		    }
		}




